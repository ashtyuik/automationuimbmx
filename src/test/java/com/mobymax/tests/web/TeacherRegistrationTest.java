package com.mobymax.tests.web;

import com.mobymax.constants.TestNgGroups;
import com.mobymax.models.TeacherRegistrationModel;
import com.mobymax.models.providers.TeacherRegistrationProvider;
import com.mobymax.tests.WebBaseTest;
import com.mobymax.web.ui.pages.teacher.TeacherHomePage;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

import static com.mobymax.web.ui.helpers.RegistrationHelper.registerNewIndependentTeacher;
import static org.assertj.core.api.Assertions.assertThat;

public class TeacherRegistrationTest extends WebBaseTest {

    private final TeacherRegistrationModel teacherRegistrationModel =
            new TeacherRegistrationProvider().getIndependentTeacherModel();

    @TmsLink("1")
    @Test(description = "Teacher registration", groups = {TestNgGroups.REGRESSION})
    public void testTeacherRegistration() {
        TeacherHomePage teacherHomePage = registerNewIndependentTeacher(teacherRegistrationModel);

        assertThat(teacherHomePage.isTeacherHomePageOpened())
                .as("Teacher home page not loaded")
                .isTrue();
    }
}
